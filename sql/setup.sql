CREATE TABLE people (
	id SERIAL PRIMARY KEY,
	name VARCHAR(256),
	role VARCHAR(256),
  	ship VARCHAR(256) DEFAULT NULL
);

INSERT INTO people (name, role, ship) VALUES ('Alice', 'Captain', 'SS Minnow');
INSERT INTO people (name, role, ship) VALUES ('Bob', 'Commander', 'SS Minnow');
INSERT INTO people (name, role, ship) VALUES ('Carol', 'Captain', 'SS Calypso');
INSERT INTO people (name, role) VALUES ('Dave', 'Ensign');
